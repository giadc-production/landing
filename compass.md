Landing Page Setup
==================

* Install [Atom](http://atom.io)
* If OS < 10.9 do the following:
  - `sudo gem update --system`
  - `sudo gem install compass`
* If OS >= 10.9, you will get an error installing compass. In this case, do the following after `sudo gem update --system`:
  - `xcode-select --install`
  - `sudo xcode-select --switch /Library/Developer/CommandLineTools/`
  - `sudo ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future gem install nokogiri -v 1.5.11`
  - `sudo gem install cupertino`
  - Then, install compass with the command above.

Testing if Compass works
========================

* A successful installation of Compass should guarantee functioning compilation these days, but just in case:
  - Download the Landing Page repo [here](https://bitbucket.org/giadc_salamander/giadc-landing-pages)
  - cd into the directory (renaming the folder is best) `cd path/to/folder`
  - once at the directory, run `compass watch`
  - Compass should activate, watch for changes, and compile the css file.