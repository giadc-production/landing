$(function() {
          $('a[rel*=modal]').leanModal({ closeButton: ".modal-close" });
          $('.close').click(function(){
            $(this).parent().hide();
          });
          $("#slider").owlCarousel({
            navigation : true, 
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            autoPlay:true,
            stopOnHover: true,
            navigationText: [
              "<i class='fa fa-arrow-circle-o-left fa-3x text-white'></i>",
              "<i class='fa fa-arrow-circle-o-right fa-3x text-white'></i>"
            ]
          });
          var headerHeight = 0;
          $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top - headerHeight
                }, 1000);
                return false;
              }
            }
          });
      });