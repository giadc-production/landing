# LANDING PAGE STYLE AND TEMPLATES #

[Documentation](http://digitalcreative.gannett.com/landing/tutorial/)

### What is this all about? ###

* This repository contains a general stylesheet, as well as template specific styles.
* It also contains SASS files to easily edit general settings.

**Each time you start a new landing page, you should download this repo to a new directory!**

### What do I need? ###

* A code editor. [Atom](http://atom.io), [Brackets](http://brackets.io) or something similar.
* [Sass](http://sass-lang.com)
* [Compass](http://compass-style.org), while optional, compiles a lot easier than plain sass.

### Setup ###
1. I would suggest making a landing page folder somewhere on your computer.
2. Go to that folder in terminal:
```
cd ~/[path]/[to]/[folder]
```
3. You are going to create a new folder in your landing page directory and clone this repo into it:
```
git clone https://gitlab.com/giadc-production/landing.git [new folder name]
```
4. To start editing code, and compiling the sass (.scss) files into a css file, do this if you have compass installed --> be in your new directory in terminal and enter:
```
compass watch
```
If you have only sass installed without compass, enter in terminal:
```
sass --watch sass:css
```
5. To stop the watching/compiling process for both sass and compass, hit `control+C` in terminal.


### Great, I need to make sense of these files! ###

* [Training](http://digitalcreative.gannett.com/landing/tutorial/) will show you which files are important. custom.scss is a file that generates a css -- not tied to a specific template.
* [Bootstrap](http://getbootstrap.com) takes care of all of the general styles, and as it the files are linked from their CDN servers, there are no additional css or js files to upload past the custom.css. There are backup CSS and JS files just in case, though.
